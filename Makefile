all:
	g++ -fPIC -c paint.cpp -o paint.o -g
	g++ -fPIC -c car.cpp -o car.o -g
	g++ -fPIC -c fbdev.cpp -o fbdev.o -g
	g++ -fPIC -c road.cpp -o road.o -g

	g++ -shared -Wl,-soname,libpaint.so -o libpaint.so paint.o
	g++ -shared -Wl,-soname,libcar.so -o libcar.so car.o
	g++ -shared -Wl,-soname,libfbdev.so -o libfbdev.so fbdev.o
	g++ -shared -Wl,-soname,libroad.so -o libroad.so road.o
	g++ -o game game.cpp -g -L${PWD} -lpaint -lcar -lfbdev -lncurses -lroad

game:
	g++ -o game game.cpp -g -L${PWD} -lpaint -lcar -lfbdev -lncurses -lroad

clean:
	rm -rf game libpaint.so libcar.so libfbdev.so libroad.so *.o

install:
	sudo cp libpaint.so /usr/lib/
	sudo cp libcar.so /usr/lib/
	sudo cp libfbdev.so /usr/lib/
	sudo cp libroad.so /usr/lib/

