#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>

#include "paint.hpp"
#include "fbdev.hpp"
#include "car.hpp"
#include "road.hpp"

static uint32_t clr_val[] = {
        0, /*black */
        0x00FF0000, /* Red */
        0x0000FF00, /* Green */
        0x000000FF, /* Blue */
        0xFFFFFFFF, /* White */
};

int setup_fbdev(fbdev *fbd)
{
    int ret;

    ret = fbd->open_fbdev();
    if (ret < 0) {
        printf("Cant open fb\n");
        return ret;
    }

    ret = fbd->get_varinfo();
    if (ret < 0) {
        printf("Can't get varinfo\n");
        fbd->close_fbdev();
        return ret;
    }

    fbd->varinfo.grayscale = 0;
    fbd->varinfo.bits_per_pixel = 32;
    ret = fbd->set_varinfo();
    if (ret < 0) {
        printf("Can't set varinfo\n");
        fbd->close_fbdev();
        return ret;
    }

    ret = fbd->get_varinfo();
    if (ret < 0) {
        printf("Can't get new varinfo\n");
        fbd->close_fbdev();
        return ret;
    }

    ret = fbd->get_screensize();
    if (ret < 0) {
        printf("Can't get sceensz\n");
        fbd->close_fbdev();
        return ret;
    }

    printf("Fbdev setup done\n");
    return 0;
}

static struct termios new_kbd_mode;
static struct termios g_old_kbd_mode;
void setup_terminal(void)
{
    char c;

    /* put keyboard (stdin) in raw, unbuffered mode */
    tcgetattr (0, &g_old_kbd_mode);
    memcpy (&new_kbd_mode, &g_old_kbd_mode, sizeof (struct termios));

    new_kbd_mode.c_lflag &= ~(ICANON | ECHO);
    new_kbd_mode.c_cc[VTIME] = 0;
    new_kbd_mode.c_cc[VMIN] = 1;
    tcsetattr (0, TCSANOW, &new_kbd_mode);
}

static void reset_terminal(void)
{
   /* set back into old mode */
    tcsetattr (0, TCSANOW, &g_old_kbd_mode);
}

static inline char get_term_input(void)
{
    char c;

    if (read (0, &c, 1) != 1)
        c = '\0';
    return c;
}

static void show_animation(paint *p)
{
    p->paint_a_buffer_region_white(100, 100, 100, 100);
    sleep(1);
    p->paint_a_buffer_region(200, 200, 100, 100, p->hash_get_clr_val(red));
    sleep(1);
    p->paint_a_buffer_region(300, 300, 100, 100, p->hash_get_clr_val(green));
    sleep(1);
    p->paint_a_buffer_region(400, 400, 100, 100, p->hash_get_clr_val(blue));
    sleep(1);
    p->paint_a_buffer_region(500, 300, 100, 100, p->hash_get_clr_val(green));
    sleep(1);
    p->paint_a_buffer_region(600, 200, 100, 100, p->hash_get_clr_val(red));
    sleep(1);
    p->paint_a_buffer_region_white(700, 100, 100, 100);
    sleep(1);
    p->clear_screen();
}

int main()
{
    car c;
    paint p;
    fbdev screen;
    road r;
    int ret;

    memset(&screen, 0, sizeof(screen));
    memset(&c, 0, sizeof(c));
    memset(&p, 0, sizeof(p));
    memset(&r, 0, sizeof(r));

    ret = setup_fbdev(&screen);
    if (ret) {
        printf("Setup fb failed\n");
        return ret;
    }

    /* map and init paint buffer using screen fb */
    screen.fbp = (uint8_t *)mmap(0, screen.screensize, PROT_READ | PROT_WRITE, MAP_SHARED,
				screen.fb_fd, (off_t)0);
    if (!screen.fbp) {
        printf("Can't map fb\n");
        screen.close_fbdev();
        return ret;
    }

    /* Init main screen */
    p.gfb.buf = screen.fbp;
    p.gfb.xres = screen.varinfo.xres;
    p.gfb.yres = screen.varinfo.yres;
    p.gfb.bpp = screen.varinfo.bits_per_pixel/8;
    p.gfb.xoff = 0;
    p.gfb.yoff = 0;

    p.init_clr_hash(5, clr_val);
    p.clear_screen();
    show_animation(&p);

    /* Draw the road on screen */
    r.h = screen.varinfo.xres/3;
    r.v = screen.varinfo.yres;
    r.xoff = (screen.varinfo.xres/3);
    r.yoff = 0;
    r.clr = ROAD_CLR;
    r.draw_road(&p);

    /* init car position at bottom-middle of the screen */
    c.pos.x = screen.varinfo.xres/2;
    c.pos.y = screen.varinfo.yres - 20;
    printf("Car start co-ordinates: %dx%d\n", c.pos.x, c.pos.y);

    /* Paint initial position of car */
    p.draw_block(c.pos.x, c.pos.y, CAR_SZ_H_PIXELS, CAR_SZ_V_PIXELS, CAR_CLR);
    sleep(1);

    setup_terminal();

    while(1) {
        char in = get_term_input();

        /* clean old pixel first*/
        p.draw_block(c.pos.x, c.pos.y, CAR_SZ_H_PIXELS, CAR_SZ_V_PIXELS, ROAD_CLR);

        switch(in) {
            case 'w':
            c.move_up(screen.varinfo.yres);
            break;

            case 'z':
            c.move_down(screen.varinfo.yres);
            break;

            case 'd':
            c.move_right(screen.varinfo.xres);
            break;

            case 'a':
            c.move_left(screen.varinfo.xres);
            break;

            case 't':
            case 'q':
            case '\0':
            goto close;
        }

        /* set paint pixel */
        p.draw_block(c.pos.x, c.pos.y, CAR_SZ_H_PIXELS, CAR_SZ_V_PIXELS, CAR_CLR);
    }

close:
    reset_terminal();
    p.delete_clr_hash();
    munmap(screen.fbp, screen.screensize);
    screen.close_fbdev();
    return 0;
}
