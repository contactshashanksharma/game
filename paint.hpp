/*
 * Copyright 2022 Shashank Sharma (contactshashanksharma@gmail.com)
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef PAINT_HPP
#define PAINT_HPP
#include <stdint.h>
#define MAX_CLR_SUPPORTED 255

struct fb {
	int x;
	int y;
	int d;
	int handle;
	int fb_fd;
	uint32_t stride;
	uint32_t size;
	char *mapped_fb;
};

struct game_fb {
    uint8_t *buf;
    int bpp;
    uint32_t xres;
    uint32_t yres;
    uint32_t xoff;
    uint32_t yoff;
};

struct pixel {
    uint32_t x;
    uint32_t y;
};

struct clr_hash_data {
	uint8_t clr;
	uint64_t val;
};

struct clr_hash_table {
	struct clr_hash_data *cval[MAX_CLR_SUPPORTED];
	int entries;
};

enum color {
	black = 0,
	red,
	green,
	blue,
	white,
    color_max,
};

class paint {
    public:
        struct game_fb gfb;
		struct game_fb obs;

		void paint_a_buffer_region(int x_off, int y_off, int h, int v, int val);
		void paint_a_buffer_region_white(int x_off, int y_off, int h, int v);
		void paint_a_buffer_region_black(int x_off, int y_off, int h, int v);

		void clear_screen(void);
        void draw_block(int x, int y, int hsz, int vsz, enum color clr);
		void update_screen(void);

		void init_clr_hash(int num, uint32_t *clr_val);
		void delete_clr_hash(void);
		uint32_t hash_get_clr_val(int key);
};
#endif