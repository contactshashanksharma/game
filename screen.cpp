#include <stdint.h>
#include "paint.hpp"

class obst {

};

class main_screen {

public:
    uint32_t size_h;
    uint32_t size_v;
    uint32_t hactive;
    uint32_t vactive;

    void draw_obst(paint *p);
    void draw_decor(paint *p);
    
};