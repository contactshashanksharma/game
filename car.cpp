#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "car.hpp"

#define VELOCITY 4

void car::move_up(uint32_t yres)
{
    pos.y -= VELOCITY;
    /* clip */
    if (pos.y < CAR_SZ_V_PIXELS)
        pos.y = CAR_SZ_V_PIXELS;
}

void car::move_down(uint32_t yres)
{
    pos.y += VELOCITY;
    if (pos.y > yres - CAR_SZ_V_PIXELS)
        pos.y = yres;
}

void car::move_right(uint32_t xres)
{
    pos.x += VELOCITY;
    if (pos.x > xres - CAR_SZ_H_PIXELS)
        pos.y = xres;
}

void car::move_left(uint32_t xres)
{
    pos.x -= VELOCITY;
    if (pos.x < CAR_SZ_H_PIXELS)
        pos.y = CAR_SZ_H_PIXELS;
}
