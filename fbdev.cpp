#include "fbdev.hpp"

int fbdev::get_varinfo(void)
{
	int ret; 

	/* Get variable screen info */
	ret = ioctl(fb_fd, FBIOGET_VSCREENINFO, &varinfo);
	if (ret < 0) {
		printf("Failed to get fbdev varinfo\n");
		close(fb_fd);
	}

	return ret;
}

int fbdev::set_varinfo(void)
{
	int ret;
	varinfo.grayscale=0;
	varinfo.bits_per_pixel=32;

	/* Set variable screen info for new BPP */
	ret = ioctl(fb_fd, FBIOPUT_VSCREENINFO, &varinfo);
	if (ret < 0) {
		printf("Failed to set bpp\n");
		close(fb_fd);
	}

	return ret;
}

int fbdev::get_screensize(void)
{
	int ret;

	ret = ioctl(fb_fd, FBIOGET_FSCREENINFO, &fixinfo);
	if (ret < 0) {
		printf("Failed to get fb fix info\n");
		close(fb_fd);
		return ret;
	}

	screensize = varinfo.yres_virtual * fixinfo.line_length;
	return 0;
}

int fbdev::open_fbdev(void)
{
	fb_fd = open("/dev/fb0", O_RDWR);
	if (fb_fd < 0) {
		printf("Failed to open fbdev 0\n");
		return -1;
	}

	return 0;
}

void fbdev::close_fbdev(void)
{
	close(fb_fd);
}
