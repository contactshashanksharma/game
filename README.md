# Game
This is a simple C++ based skeleton of a car race game, which uses framebuffer device for disply and CPU for rendering.
This is nothing more than a homework project just to keep the design skills fresh and in no way a render efficient code, coz it uses CPU based render on a basic framebuffer.

The long term idea is to:
- replace each of the CPU rendered block with a GPU rendered efficient subsection of the code, but keeping the logic same.
- advance the game with added complexity and stages.
- introduce more obstacles and bonuses in between.
- learn and make things better.

Building:
=========
- Read the makefile once, or just do:
  $ make clean && make && sudo make install

Running:
==========
- Switch to fb_console with (chvt 4) or (ctrl + alt + 4)
- ./game

