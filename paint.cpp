/*
 * Copyright 2022 Shashank Sharma (contactshashanksharma@gmail.com)
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>	
#include <malloc.h>
#include <wchar.h>
#include <sys/time.h>
#include "paint.hpp"

static struct clr_hash_table *table;

/* ============ Color hash table related functions =========== */

static inline int hash_get_index(int key)
{
	return key % MAX_CLR_SUPPORTED;
}

uint32_t paint::hash_get_clr_val(int key)
{
	int index;

	if (!table || key < 0 || key > table->entries) {
		printf("Invalid input\n");
		return -1;
	}

	index = hash_get_index(key);
	return table->cval[index]->val;
}

void paint::delete_clr_hash(void)
{
	int i = 0;

	for (i = 0; i < table->entries; i++)
		free(table->cval[i]);
	free(table);
}

void paint::init_clr_hash(int num, uint32_t *clr_val)
{
	int i;

	table = (struct clr_hash_table *)malloc(sizeof(struct clr_hash_table));
	for (i = 0; i < num; i++) {
		table->cval[i] = (struct clr_hash_data *)malloc(sizeof(struct clr_hash_data));
		table->cval[i]->clr = i;
		table->cval[i]->val = clr_val[i];		
	}
	table->entries = num;
}

/* ============ Drawing functions =========== */

static void _paint_x(char *buf, int xres, int x, uint32_t val)
{
	uint32_t *pixel = (uint32_t *)buf;

	if (x < xres) {
		pixel[x++] = val;
		_paint_x(buf, xres, x, val);
	}
}

/* Use custom value of pitch to paint buffer region */
static void _paint_y(char *fb, int xres, int yres, uint32_t pitch,
		     int y, int bpp, uint32_t val)
{
	if (y < yres) {
		_paint_x(fb + y++ * pitch, xres, 0, val);
		_paint_y(fb, xres, yres, pitch, y, bpp, val);
	}
}

static void paint_buf_recursively(char *buf, uint32_t xres, uint32_t yres,
				  uint32_t pitch, int bpp, int val)
{
	if (!buf)
		return;
	_paint_y(buf, xres, yres, pitch, 0, bpp, val);
}

void paint::paint_a_buffer_region(int x_off, int y_off, int h, int v, int val)
{
	uint32_t pitch = gfb.xres * gfb.bpp;
	char *sb = (char *)(gfb.buf + y_off * pitch + x_off * gfb.bpp);

	paint_buf_recursively(sb, h, v, pitch, gfb.bpp, val);
}

void paint::paint_a_buffer_region_white(int x_off, int y_off, int h, int v)
{
	uint32_t pitch = gfb.xres * gfb.bpp;
	char *sb = (char *)(gfb.buf + y_off * pitch + x_off * gfb.bpp);

	paint_buf_recursively(sb, h, v, pitch, gfb.bpp, hash_get_clr_val(white));
}

void paint::paint_a_buffer_region_black(int x_off, int y_off, int h, int v)
{
	uint32_t pitch = gfb.xres * gfb.bpp;
	char *sb = (char *)(gfb.buf + y_off * pitch + x_off * gfb.bpp);

	paint_buf_recursively(sb, h, v, pitch, gfb.bpp, hash_get_clr_val(black));
}

void paint::clear_screen(void)
{
    paint_a_buffer_region_black(0, 0, gfb.xres, gfb.yres);
}

void paint::draw_block(int x, int y, int hsz, int vsz, enum color clr)
{
    uint32_t pitch = gfb.xres * gfb.bpp;
	uint32_t sub_pitch = hsz * gfb.bpp;
	uint32_t *car = (uint32_t *)(gfb.buf + y * pitch + x * gfb.bpp);
	uint32_t clrval = hash_get_clr_val(clr);

	paint_a_buffer_region(x, y, hsz, vsz, clrval);
}