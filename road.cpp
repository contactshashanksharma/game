/*
 * Copyright 2022 Shashank Sharma (contactshashanksharma@gmail.com)
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#include "road.hpp"

void obst::blast(paint *p)
{
    struct game_fb *old;

    p->paint_a_buffer_region(pos.x, pos.y, OBS_SZ_X/4, OBS_SZ_Y/4, p->hash_get_clr_val(red));
    p->paint_a_buffer_region(pos.x + OBS_SZ_X/4, pos.y + OBS_SZ_Y/4,
                OBS_SZ_X/4, OBS_SZ_Y/4, p->hash_get_clr_val(green));
    p->paint_a_buffer_region(pos.x + OBS_SZ_X/2, pos.y + OBS_SZ_Y/2,
                OBS_SZ_X/4, OBS_SZ_Y/4, p->hash_get_clr_val(blue));
    p->paint_a_buffer_region(pos.x + OBS_SZ_X * 3/4, pos.y + OBS_SZ_Y * 3/4,
                OBS_SZ_X/4, OBS_SZ_Y/4, p->hash_get_clr_val(white));
}

void road::draw_obst(paint *p)
{
    int obs_off = v/NUM_OBS_ZONES;
    int i;

    for (i = 0; i < NUM_OBS_ZONES; i++) {
        /* left side obst */
        p->draw_block(xoff + xoff/2 - 1 * OBS_SZ_X, i * obs_off, OBS_SZ_X,
                OBS_SZ_Y, OBST_CLR);
        /* Right side obst */
        p->draw_block(xoff + xoff/2 + 1 * OBS_SZ_X, i * obs_off + obs_off/2,
                OBS_SZ_X, OBS_SZ_Y, OBST_CLR);
    }
}

void road::draw_road(paint *p)
{
    obst o_even, o_odd;

    p->paint_a_buffer_region(xoff, yoff, h, v, p->hash_get_clr_val(clr));
    draw_obst(p);
}
