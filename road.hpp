/*
 * Copyright 2022 Shashank Sharma (contactshashanksharma@gmail.com)
 * Permission is hereby granted, free of charge, to any person obtaining a
 * copy of this software and associated documentation files (the "Software"),
 * to deal in the Software without restriction, including without limitation
 * the rights to use, copy, modify, merge, publish, distribute, sublicense,
 * and/or sell copies of the Software, and to permit persons to whom the
 * Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL
 * THE COPYRIGHT HOLDER(S) OR AUTHOR(S) BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 *
 */
#ifndef ROAD_HPP
#define ROAD_HPP
#include <stdint.h>
#include "paint.hpp"

#define OBS_SZ_X 50
#define OBS_SZ_Y 5
#define NUM_OBS_ZONES 10
#define ROAD_CLR blue
#define OBST_CLR white

class obst {
    public:
        int num_obj;
        struct game_fb fb;
        struct pixel pos;

        void generate(int scr_x, int src_y, paint *p);
        void blast(paint *p);
};

class road {
    public:
        uint32_t h;
        uint32_t v;
        uint32_t xoff;
        uint32_t yoff;
        enum color clr;

        void draw_road(paint *p);
        void draw_obst(paint *p);
};

#endif